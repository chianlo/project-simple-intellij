import model.Person;
import utils.Logger;

/**
 * @author Roberto Casadei
 */

public class Main {
    public static final String WELCOME_MESSAGE = "Hello ";

    public static void main(String[] args){
        //no singole lettere per variabili: l -> logger; p -> person;
        //no valori "magici": parametri di Person passati tramite args e messaggio di log preso da costante
        Logger logger = new Logger();
        Person person = new Person(args[0], Integer.parseInt(args[1]));
        logger.loggerPrintString(WELCOME_MESSAGE + person);
    }
}
