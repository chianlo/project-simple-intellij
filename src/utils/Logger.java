package utils;

/**
 * @author Roberto Casadei
 */

public class Logger {
    public static final String PRINT_MESSAGE = "[Print] %s\n";
    // niente abbreviazioni: log -> logger
    public static void loggerPrintString(String s){
        System.out.format(PRINT_MESSAGE,s);
    }

    public static void loggerPrintStrings(String[] array){
        for (String s: array){
            System.out.format(PRINT_MESSAGE,s);
        }
    }
}
